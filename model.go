package main

import (
	"math"
	"math/rand"
	"time"
	"strings"
	"log"
	"strconv"
)


type Param struct {
	period time.Duration
	speed, stdev float64	
}

type ParamList struct {
	start time.Time
	params []Param
	total_period time.Duration
}

func (p *Param) GetSpeed() float64 {
	return math.Max(0.0, rand.NormFloat64() * p.stdev + p.speed)
}

func (p *ParamList) GetSpeed() float64 {
	now := time.Now()
	d := now.Sub(p.start) % p.total_period
	for _, e := range p.params {
		if d < e.period {
			return e.GetSpeed()
		} else {
			d -= e.period
		}
	}
	return p.params[len(p.params)-1].GetSpeed()
}

func ParseParamString(s string) *ParamList {
	p := new (ParamList)
	start_sep := strings.Index(s, "@")
	if start_sep != -1 {
		res, err := time.Parse(time.RFC3339, s[0:start_sep])
		p.start = res
		if err != nil {
			log.Fatalf("Error parsing time: %s", err)
		}
		if p.start.After(time.Now()) {
			log.Fatal("Starting time should be in the past.")
		}
	} else {
		p.start = time.Now()
	}
	
	entries := strings.Split(s[start_sep + 1:], "|")
	p.params = make([]Param, len(entries))
	p.total_period = time.Duration(0)

	for i, e := range entries {
		v := strings.Split(e, ",")
		
		p.params[i] = Param{time.Duration(365 * 24) * time.Hour, 10, 0}

		if len(v) >= 1 {
			speed, err := strconv.ParseFloat(v[0], 64)
			p.params[i].speed = speed
			if err != nil {
				log.Fatalf("Error in parsing average speed: %s", err)
			}
		}
		if len(v) >= 2 {
			stdev, err := strconv.ParseFloat(v[1], 64)
			p.params[i].stdev = stdev
			if err != nil {
				log.Fatalf("Error in parsing speed spread: %s", err)
			}
		}
		if len(v) >= 3 {
			period, err := time.ParseDuration(v[2])
			p.params[i].period = period
			if err != nil {
				log.Fatalf("Error in parsing duration: %s", err)
			}
		}
		p.total_period += p.params[i].period
	}

	return p
}