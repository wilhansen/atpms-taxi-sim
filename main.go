package main

import (
	"./polyline"
	"bufio"
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strconv"
	"sync"
	"time"
)

var _ = io.Pipe

const (
	ROUTE_TEMPLATE            = "http://%v/route?src=%v,%v&dst=%v,%v&k=1"
	ROUTE_OSRM_TEMPLATE       = "http://%v/viaroute?loc=%v,%v&loc=%v,%v"
	SEND_TEMPLATE             = "http://%v/api/logs"
	SEND_TEMPLATE_WH          = "http://%s/api/locationlogs/save_bulk"
	EARTH_RADIUS              = 6371.0
	OSRM_PRECISION_ADJUSTMENT = 1e-1
)

type AgentLocation struct {
	Lon, Lat float64
	AgentId  uint64
}

type Point struct {
	Lon, Lat float64
}

type AgentInfo struct {
	id           string
	access_token string
}

func toRad(deg float64) float64 {
	return deg * math.Pi / 180.0
}

func dist(a Point, b Point) float64 {
	dlat := toRad(b.Lat - a.Lat)
	dlon := toRad(b.Lon - a.Lon)
	mlat := toRad((b.Lat + a.Lat) * 0.5)

	c := math.Cos(mlat) * dlon
	return EARTH_RADIUS * math.Sqrt(dlat*dlat+c*c)
}

func lerp(a Point, b Point, t float64) Point {
	return Point{a.Lon + (b.Lon-a.Lon)*t, a.Lat + (b.Lat-a.Lat)*t}
}

func lerper(points []Point) (func(float64) Point, float64) {
	totalDist := 0.0
	offsets := make([]float64, len(points)-1)

	for i := 1; i < len(points); i++ {
		offsets[i-1] = dist(points[i], points[i-1])
		totalDist += offsets[i-1]
	}

	for i := 1; i < len(offsets); i++ {
		offsets[i] += offsets[i-1]
	}

	return func(t float64) Point {
		i := sort.SearchFloat64s(offsets, t)
		base := 0.0
		if i > len(offsets)-1 {
			i = len(offsets) - 1
		}
		if i > 0 {
			base = offsets[i-1]
		}

		return lerp(points[i], points[i+1], (t-base)/(offsets[i]-base))
	}, totalDist
}

var sendHost string
var routeHost string
var nodeList []Point
var updateInterval time.Duration
var startupDelaySpreadFactor float64
var isOSRM bool
var isWarehouse bool
var warehouseSendAsForm bool
var verbose uint
var speedParam *ParamList

func runAgent(agentInfo *AgentInfo, wg *sync.WaitGroup, shouldExit *bool) {
	if verbose > 0 {
		log.Printf("Agent %s up!", agentInfo.id)
	}
	defer wg.Done()

	time.Sleep(time.Duration(rand.Float64() * float64(updateInterval.Nanoseconds()) * startupDelaySpreadFactor))

	src_index := rand.Intn(len(nodeList))
	dst_index := rand.Intn(len(nodeList))
	src := nodeList[src_index]
	dst := nodeList[dst_index]

	for !*shouldExit {

		if verbose > 0 {
			log.Printf("Agent %s: going from (%f, %f) to (%f, %f)\n", agentInfo.id, src.Lon, src.Lat, dst.Lon, dst.Lat)
		}


		var path []Point
		if isOSRM {
			resp, err := http.Get(fmt.Sprintf(ROUTE_OSRM_TEMPLATE, routeHost, src.Lat, src.Lon, dst.Lat, dst.Lon))
			if err != nil {
				log.Printf("Failed retrieving route: %v", err)
				if resp != nil && resp.Body != nil {
					io.Copy(ioutil.Discard, resp.Body)
					resp.Body.Close()
				}
				time.Sleep(time.Duration(float64(updateInterval.Nanoseconds()) * startupDelaySpreadFactor))
				continue
			}

			var osrmResponse struct {
				Route_geometry string `json:"route_geometry"`
			}

			decoder := json.NewDecoder(resp.Body)

			err = decoder.Decode(&osrmResponse)

			resp.Body.Close()

			if err != nil && err != io.EOF {
				log.Printf("Error parsing received route: %v", err)
				continue
			}

			decoded, err := polyline.Decode(osrmResponse.Route_geometry, 2)
			if err != nil {
				log.Fatalf("Error parsing returned polyline: %v", err)
			}

			path = make([]Point, 1)
			for i := 0; i < len(decoded); i += 2 {
				path = append(path, Point{decoded[i+1] * OSRM_PRECISION_ADJUSTMENT, decoded[i] * OSRM_PRECISION_ADJUSTMENT})
			}
		} else {

			type Path struct {
				Eta float64
				Distance float64
				Nodes []Point
			}
			type RouteResponse struct {
				Paths []Path
			}

			var rr RouteResponse

			resp, err := http.Get(fmt.Sprintf(ROUTE_TEMPLATE, routeHost, src.Lon, src.Lat, dst.Lon, dst.Lat))
			if err != nil {
				log.Printf("Failed retrieving route: %v", err)
				if resp != nil && resp.Body != nil {
					resp.Body.Close()
				}
				time.Sleep(time.Duration(float64(updateInterval.Nanoseconds()) * startupDelaySpreadFactor))
				continue
			}

			decoder := json.NewDecoder(resp.Body)

			err = decoder.Decode(&rr)

			resp.Body.Close()

			if err != nil && err != io.EOF {
				log.Printf("Error parsing received route: %v", err)
				continue
			}
			if len(rr.Paths) != 0 {
				path = rr.Paths[0].Nodes
			} else {
				path = nil;
			}

		}

		if path == nil || len(path) == 0 {
			src_index = rand.Intn(len(nodeList))
			dst_index = rand.Intn(len(nodeList))
			src = nodeList[src_index]
			dst = nodeList[dst_index]
			continue
		}

		l, totalDist := lerper(path)

		if verbose > 0 {
			log.Printf("Agent %s: received route. Total Distance: %fkm", agentInfo.id, totalDist)
		}

		timestamp := time.Now()
		session_token := fmt.Sprintf("%s:%d-%d-%x.%x", agentInfo.id, src_index, dst_index, timestamp.Unix(), timestamp.Nanosecond())

		tick := 0.0
		for d := 0.0; d < totalDist; d += tick {
			if *shouldExit {
				return
			}

			p := l(d)

			if verbose > 1 {
				log.Printf("Agent %s: sending coordinates: (%f, %f) - %s", agentInfo.id, p.Lon, p.Lat, session_token)
			}

			if isWarehouse {
				type LogEntry struct {
					Lon       float64 `json:"lon"`
					Lat       float64 `json:"lat"`
					Timestamp int64   `json:"timestamp"`
					User_id   string  `json:"user_id"`
				}

				type SingleLogEntry struct {
					Logs [1]LogEntry `json:"locationlogs"`
				}

				var sendData SingleLogEntry
				sendData.Logs[0] = LogEntry{p.Lon, p.Lat, time.Now().Unix(), agentInfo.id}

				var res *http.Response
				var err error

				if warehouseSendAsForm {
					m, _ := json.Marshal(sendData)
					msg := string(m)
					if verbose > 0 {
						log.Printf("Sending: %v", msg)
					}
					res, err = http.PostForm(fmt.Sprintf(SEND_TEMPLATE_WH, sendHost), url.Values{"locationlogs": {msg}})
				} else {
					reader, writer := io.Pipe()
					go func() {
						encoder := json.NewEncoder(writer)
						encoder.Encode(sendData)
						writer.Close()
					}()

					if verbose > 0 {
						m, _ := json.Marshal(sendData)
						log.Printf("Sending: %v", string(m))
					}
					res, err = http.Post(fmt.Sprintf(SEND_TEMPLATE_WH, sendHost), "application/json", reader)
				}

				if err != nil {
					log.Printf("Error sending device log data: %v", err)
				}

				if res != nil && res.Body != nil {
					body, err := ioutil.ReadAll(res.Body)
					if err != nil {
						log.Printf("Error receiving data for agent %s %s", agentInfo.id, err)
					} else if res.StatusCode >= 400 {
						log.Printf("Error response[%s] (%d): %s", agentInfo.id, res.StatusCode, string(body))
					} else if verbose > 0 {
						log.Printf("Response[%s]: %s", agentInfo.id, string(body))
					}
					res.Body.Close()
				}
			} else {
				reader, writer := io.Pipe()
				type LogEntry struct {
					Lon           float64 `json:"lon"`
					Lat           float64 `json:"lat"`
					Logged_at     string  `json:"logged_at"`
					Session_token string  `json:"session_token"`
					Vessel_token  string  `json:"vessel_token"`
				}

				type SingleLogEntry struct {
					Logs [1]LogEntry `json:"logs"`
				}
				go func() {
					var sendData SingleLogEntry
					sendData.Logs[0] = LogEntry{p.Lon, p.Lat, time.Now().String(), session_token, agentInfo.access_token}

					encoder := json.NewEncoder(writer)
					encoder.Encode(sendData)
					writer.Close()
				}()

				res, err := http.Post(fmt.Sprintf(SEND_TEMPLATE, sendHost), "application/json", reader)
				reader.Close()
				if err != nil {
					log.Printf("Error sending device log data: %v", err)
				}

				if res != nil && res.Body != nil {
					body, err := ioutil.ReadAll(res.Body)
					if err != nil {
						log.Printf("Error receiving data for agent %s %s", agentInfo.id, err)
					} else if res.StatusCode >= 400 {
						log.Printf("Error response[%s] (%d): %s", agentInfo.id, res.StatusCode, string(body))
					} else if verbose > 0 {
						log.Printf("Response[%s]: %s", agentInfo.id, string(body))
					}
					res.Body.Close()
				}
			}

			time.Sleep(updateInterval)
			speed := speedParam.GetSpeed()
			tick = speed * updateInterval.Hours()
		}

		src_index = dst_index
		src = dst
		dst_index = rand.Intn(len(nodeList))
		dst = nodeList[dst_index]
	}
}

func runAgents(agentList []AgentInfo, shouldExit *bool) sync.WaitGroup {
	var wg sync.WaitGroup

	for i := 0; i < len(agentList); i++ {
		wg.Add(1)
		go runAgent(&agentList[i], &wg, shouldExit)
	}

	log.Printf("Started %d agents", len(agentList))
	return wg
}

func jsonMessage(message string) []byte {
	b, _ := json.Marshal(struct {
		Message string `json:"message"`
	}{message})
	return b
}
func setJsonHeader(w http.ResponseWriter) {
	w.Header()["Content-Type"] = []string{"application/json"}
}

func runServer(port uint, token string, agentList []AgentInfo) {
	var shouldExit *bool = nil

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		index, err := os.Open("index.html")
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("index.html not found. Verify that it's in the application directory."))
		} else {
			w.Header()["Content-Type"] = []string{"text/html"}
			io.Copy(w, index)
		}
	})

	http.HandleFunc("/control", func(w http.ResponseWriter, r *http.Request) {
		if r.FormValue("token") != token {
			setJsonHeader(w)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write(jsonMessage("Wrong token."))
			return
		}

		if len(r.FormValue("command")) > 0 {
			command := r.FormValue("command")
			if command == "start" {
				if shouldExit != nil {
					*shouldExit = true
					shouldExit = nil
				}

				shouldExit = new(bool)
				*shouldExit = false
				if verbose > 0 {
					log.Printf("\"start\" command received.")
				}
				runAgents(agentList, shouldExit)
				setJsonHeader(w)
				w.Write(jsonMessage("Agents started."))
			} else if command == "stop" {
				if verbose > 0 {
					log.Printf("\"stop\" command received.")
				}
				if shouldExit != nil {
					*shouldExit = true
					shouldExit = nil
				}
				setJsonHeader(w)
				w.Write(jsonMessage("Agents stopped."))
			} else {
				setJsonHeader(w)
				w.WriteHeader(http.StatusBadRequest)
				w.Write(jsonMessage("Unknown command."))
			}
		} else {
			setJsonHeader(w)
			w.WriteHeader(http.StatusBadRequest)
			w.Write(jsonMessage("No command found."))
		}
	})

	http.HandleFunc("/status", func(w http.ResponseWriter, r *http.Request) {
		status := "running"
		if shouldExit == nil {
			status = "stopped"
		} else if *shouldExit {
			status = "stopping"
		}
		b, _ := json.Marshal(struct {
			Status string `json:"status"`
			Agents int    `json:"agents"`
		}{status, len(agentList)})
		setJsonHeader(w)
		w.Write(b)
	})

	log.Printf("Starting server at port %v", port)
	http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}

func main() {
	agents := flag.String("agents", "agents.csv", "CSV file listing agents. Needed columns: id, access_token")
	flag.StringVar(&sendHost, "dest", "localhost", "Destination host endpoint.")
	flag.StringVar(&routeHost, "route", "localhost", "Router host.")
	nodes := flag.String("nodes", "nodes.txt", "List of nodes to choose from in \"lon lat\" format.")
	flag.DurationVar(&updateInterval, "interval", 100*time.Millisecond, "Interval to send updates.")
	flag.Float64Var(&startupDelaySpreadFactor, "spread", 5, "Spread of the startup delay of agents as a factor of the update interval.")
	flag.BoolVar(&isOSRM, "osrm", false, "Router is OSRM.")
	flag.BoolVar(&isWarehouse, "wh", false, "Destination is data warehouse.")
	flag.BoolVar(&warehouseSendAsForm, "wh-as-form", false, "Send warehouse data as a form (instead of a direct json body).")
	flag.UintVar(&verbose, "verbose", 0, "Verbosity level [0,2].")
	speedString := flag.String("speed", "2013-06-01T00:00:00+08:00@30,1,8h|45,1,8h", "Set speed ([start_time@]speed0[,spread0[,duration0[|speed1...]]]). If no start_time is specified, defaults to now.")
	port := flag.Uint("port", 0, "Port to host (0 to disable).")
	token := flag.String("token", "", "Token to check for control requests (blank to disable).")
	help := flag.Bool("help", false, "Prints help.")
	flag.Parse()

	if *help {
		fmt.Printf("Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		return
	}

	nodeFile, err := os.Open(*nodes)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(nodeFile)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		lon, err := strconv.ParseFloat(scanner.Text(), 64)
		if err != nil {
			log.Fatal(err)
		}
		scanner.Scan()
		lat, err := strconv.ParseFloat(scanner.Text(), 64)
		if err != nil {
			log.Fatal(err)
		}

		nodeList = append(nodeList, Point{lon, lat})
	}

	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}

	nodeFile.Close()

	var agentList []AgentInfo

	{
		agentFile, err := os.Open(*agents)

		ar := csv.NewReader(agentFile)

		header, err := ar.Read()
		if err != nil {
			log.Fatal(err)
		}

		var id_index, access_token_index int = -1, -1

		for i := 0; i < len(header); i++ {
			if header[i] == "id" {
				id_index = i
			} else if header[i] == "access_token" {
				access_token_index = i
			}
		}

		if id_index < 0 {
			log.Fatal("No id field in agents file found.")
		}
		if access_token_index < 0 {
			log.Fatal("No access_token field in agents file found.")
		}

		for {
			record, err := ar.Read()
			if err == io.EOF {
				break
			} else if err != nil {
				log.Fatal(err)
			}

			agentList = append(agentList, AgentInfo{record[id_index], record[access_token_index]})
		}
	}

	speedParam = ParseParamString(*speedString)

	if *port > 0 && len(*token) > 0 {
		runServer(*port, *token, agentList)
	} else {
		wg := runAgents(agentList, new(bool))
		wg.Wait()
	}

}
