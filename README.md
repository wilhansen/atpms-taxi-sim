#ATPMS Taxi Agent Simulator

Load simulator for ATPMS

##Building
Requires Google Go 1.3+. To build, simply run `go build`

##Usage

Pass `-help` to see the list of flags. The following are the important flags:

  * `agents` - Specifies the agentfile. See the agent.csv file for sample. The only required fields are `id` and `access_token`.
  * `dest` — The hostname of the destination endpoint. 
  * `route` — The hostname of the router endpoint.
  * `nodes` – Specifies a whitespace-separated list of nodes for which the agents to randomly pick from. The coordinates are in the format `lon lat`. e.g. `121.078251 14.6379733`

If the router endpoint is OSRM, `-osrm=true` needs to be passed too.

##Examples

Assuming the program is compiled as `taxi_sim` (but usually, it's compiled as `atpms-taxi-sim`) here are example parameters:

    ./taxi_sim -agents=2agents.csv -dest="106.186.124.150:8082" -route="173.255.242.74:8081" -osrm=true -interval=2s -nodes="nodes.txt"
    ./taxi_sim -agents=10agents.csv -dest="106.186.124.150:8082" -route="173.255.242.74:8082" -interval=2s -spread=10 -nodes="nodes.txt"
    ./taxi_sim -agents=1agents.csv -dest="127.0.0.1:9000" -route="173.255.242.74:8081" -osrm=true -interval=2s -spread=20 -nodes="nodes.txt"